<div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
    <div class="logo"><a href="" class="simple-text logo-normal">
            H2 Shop
        </a></div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item ">
                <a class="nav-link" href="./dashboard.html">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            
            <li class="nav-item ">
                <a class="nav-link" href="{{route('users.index')}}">
                    <i class="material-icons">person</i>
                    <p>Users</p>
                </a>
            </li>
            
            <li class="nav-item ">
                <a class="nav-link" href="{{route('categories.index')}}"> 
                    <i class="material-icons">content_paste</i>
                    <p>Category</p>
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="{{route('products.index')}}">
                    <i class="material-icons">content_paste</i>
                    <p>Product</p>
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="{{route('roles.index')}}">
                    <i class="material-icons">content_paste</i>
                    <p>Role</p>
                </a>
            </li>
        </ul>
    </div>
</div>
