<!--   Core JS Files   -->
<script src="{{ asset('manager/asset/js/jquery.min.js') }}"></script>
<script src="{{ asset('manager/asset/js/popper.min.js') }}"></script>
<script src="{{ asset('manager/asset/js/bootstrap-material-design.min.js') }}"></script>
<script src="{{ asset('manager/asset/js/perfect-scrollbar.jquery.min.js') }}"></script>
<!-- Plugin for the momentJs  -->
<script src="{{ asset('manager/asset/js/moment.min.js') }}"></script>
<!--  Plugin for Sweet Alert -->
<script src="{{ asset('manager/asset/js/sweetalert2.js') }}"></script>
<!-- Forms Validations Plugin -->
<script src="{{ asset('manager/asset/js/jquery.validate.min.js') }}"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="{{ asset('manager/asset/js/jquery.bootstrap-wizard.js') }}"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="{{ asset('manager/asset/js/bootstrap-selectpicker.js') }}"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="{{ asset('manager/asset/js/bootstrap-datetimepicker.min.js') }}"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="{{ asset('manager/asset/js/jquery.dataTables.min.js') }}"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="{{ asset('manager/asset/js/bootstrap-tagsinput.js') }}"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="{{ asset('manager/asset/js/jasny-bootstrap.min.js') }}"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="{{ asset('manager/asset/js/fullcalendar.min.js') }}"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="{{ asset('manager/asset/js/jquery-jvectormap.js') }}"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{ asset('manager/asset/js/nouislider.min.js') }}"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="{{ asset('manager/asset/js/core.js') }}"></script>
<!-- Library for adding dinamically elements -->
<script src="{{ asset('manager/asset/js/arrive.min.js') }}"></script>
<!-- Chartist JS -->
<script src="{{ asset('manager/asset/js/chartist.min.js') }}"></script>
<!--  Notifications Plugin    -->
<script src="{{ asset('manager/asset/js/bootstrap-notify.js') }}"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{ asset('manager/asset/js/material-dashboard.js') }}" type="text/javascript"></script>

<!--<script src="manager/asset/demo/demo.js" type="text/javascript"></script>-->
<script src="{{ asset('manager/asset/demo/demo.js') }}" type="text/javascript"></script>
<!-- --setting-- -->

<script src="{{ asset('manager/asset/js/application.js') }}" type="text/javascript"></script>

<script>
    $(document).ready(function () {
        // Javascript method's body can be found in assets/js/demos.js
        md.initDashboardPageCharts()

        md.initVectorMap()

        md.initFormExtendedDatetimepickers();
        if ($('.slider').length != 0) {
            md.initSliders();
        }

        $('[data-toggle="popover"]').popover()

        $(document).ready(function () {
            // Initialise the wizard
            demo.initMaterialWizard()
            setTimeout(function () {
                $('.card.card-wizard').addClass('active')
            }, 600)
        })
    })
</script>

@yield('script')
