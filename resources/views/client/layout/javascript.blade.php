<script src="{{ asset('client/libs/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('client/libs/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('client/js/main.js') }}"></script>

@yield('script')
