<link rel="stylesheet" href="{{ asset('client/libs/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('client/libs/font-awesome/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('client/css/main.css') }}">
<link rel="stylesheet" href="{{ asset('client/css/responsive.css') }}">

@yield('style')
