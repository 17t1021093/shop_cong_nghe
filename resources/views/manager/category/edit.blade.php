@extends('manager.layout.app')

@section('title', 'Edit'.$category->name)

@section('content')
   <div class="row">
        <div class="col-lg-6 mx-auto">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="post" action="{{ route('categories.update', $category) }}">
            @csrf
            @method('put')
                <div class="form-group">
                    <label for="category-title">Category Name</label>
                    <input type="text" name="name" value="{{ $category->name }}" class="form-control" id="category-title">
                </div>

                <button type="submit" class="btn btn-success"> Update </button>

            </form>
        </div>
   </div>
@endsection
