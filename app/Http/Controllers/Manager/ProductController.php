<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Traits\HandleImageTrait;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use App\Http\Requests\CreateProductRequest;
use DB;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $product;
    private $category;
    use HandleImageTrait;
    protected $path;

    public function __contruct( Product $product, Category $category) {
        $this->product = $product;
        $this->path = 'upload/';
        $this->category = $category;
    }

    public function index()
    {
        $products = Product::all();
        return view('manager.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        //$category = $this->category->all();
        return view('manager.product.add', compact('category'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)  
    {
        try {
            $image = $request->file('image');
            $data['name'] = $request->name;
            $data['price'] =$request->price;
            $data['image'] =$this->saveImage($image, $this->path);
            $data['category_id'] = $request->category_id;

            DB::table('products')->insert($data);
            
            return redirect()->route('products.index');


        } catch(Exception $exception) {
            DB::rollback();
            Log::error('Message: '. $exception->getMessage() . '---Line: ' . $exception->getLine());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}