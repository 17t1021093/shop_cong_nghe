@extends('manager.layout.app')

@section('title', 'Edit Product')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit Product</div>
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label">{{ __('image') }}</label>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('products.update') }}"  enctype="multipart/form-data" >
                        @csrf
                        @method('put')
                        <!-- select image -->
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <h4 class="title">Regular Image</h4>
                                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        <img src="{{asset('upload/'.$product->image)}}" alt="...">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                    <div>
                                              <span class="btn btn-rose btn-round btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="image">
                                              </span>
                                        <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                    </div>
                                </div>
                            </div>
                            @error('image')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>


                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label">{{ __('price') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="price" value="{{ old('price') }}"  >

                                
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label">{{ __('Category') }}</label>

                            <div class="col-md-6">
                                <select class="selectpicker"  name="category_id" >
                                    @foreach($category as $categoryItem)
                                        <option value="<?php echo $categoryItem->id ?>">{{$categoryItem->name}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
