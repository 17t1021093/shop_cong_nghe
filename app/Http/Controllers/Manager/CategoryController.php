<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Traits\HandleImageTrait;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $category;
    protected $product;


    public function __construct(Category $category, Product $product)
    {
        $this->category = $category;
        $this->product = $product  ;
    }


    public function index()
    {
        // $category = $this->category->first();
        // $product = $this->product->get()->take(5);

        // echo 'category:'.$category->name;

        // foreach ($products as $product => $value) {
        //     echo "Name";
        // }

        $categories=Category::all();

        return view('manager.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();

        return view('manager.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required|max:50'
        ]);

        $category = new Category([
           'name' => $request ->get('name'),
        ]);

        $category->save();
        return redirect()->route('categories.index')->with('message', 'tạo category thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('manager.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)   
    {
        $request->validate([
            'name'=>'required|max:50'
        ]);

        $category = Category::find($id);
        $category->name = $request->get('name');
        $category->save();
        return redirect()->route('categories.index')->with('message', 'sửa category thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();

        return redirect()->route('categories.index')->with('message', 'Xóa category thành công!');
    }

}
