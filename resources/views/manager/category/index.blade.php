@extends('manager.layout.app')

@section('title', 'Category')

@section('content')
<a href="{{ route('categories.create') }}" class="btn btn-success">Create New Category</a>

@if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif

    <table class="table table-striped mt-3">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Category ID</th>
            <th scope="col">Category Name</th>
            <th scope="col"> </th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
        <tr>
            <th scope="row">{{ $category->id }}</th>
            <td>{{ $category->name }}</td>
            <td class="table-buttons">
                <a href="{{ route('categories.edit', $category) }}" class="btn btn-primary">
                    <i class="fa fa-pencil"></i>
                </a>
                <form METHOD="post" ACTION="{{ route('categories.destroy', $category) }}" style="display: contents;">
                @csrf
                @method('DELETE')
                    <button type="submit" class="btn btn-danger">
                       <i class="fa fa-trash"></i>
                    </button>
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
@endsection
