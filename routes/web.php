<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::group(['middleware'=>'auth'],function(){

    



Route::group(['prefix'=>'manager', 'namespace'=>'Manager'],function(){

    Route::resource('users', 'UserController');/* ->middleware('check.user-is-male'); */

    Route::resource('categories', 'CategoryController'); 
    Route::resource('products', 'ProductController'); 

    Route::resource('roles', 'RoleController');

});
// });

// Auth::routes();
Route::group(['namespace'=>'Client'], function(){

    Route::get('/homeClient', 'ClientController@index')->name('homeClient');
    // Route::resource('carts', 'CartController');

});


// Route::get('/', function () {
//     return view('manager.dashboard.index');
// });

Route::get('/home', 'HomeController@index')->name('home');



