<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Traits\HandleImageTrait;
use App\User;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    use HandleImageTrait;

    protected $path;

    protected $userModel;
    protected $roleModel;


    public function __construct(User $user, Role $role)
    {
        $this->userModel = $user;
        $this->path = 'upload/';
        $this->roleModel = $role;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->userModel->with('roles')->latest('id')->paginate(5);

        return view('manager.user.user')->with( 'users',$users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->roleModel->where('name','!=','super-admin')->get();

        return view('manager.user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {

        $image = $request->file('image');
        $dataCreate = $request->all();
        $dataCreate['image'] = $this->saveImage($image, $this->path);
        $dataCreate['password'] = Hash::make($request->input('password'));

        $user = $this->userModel->create($dataCreate);

        $user->syncRoles($request->roles);

        return redirect()->route('users.index')->with('message', 'tao user thanh cong');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->userModel->with('roles')->findOrFail($id);
        $roles = $this->roleModel->where('name','!=','super-admin')->get();
        $listRoleIds = $user->roles->pluck('id')->ToArray();

        return view('manager.user.edit')->with(['user'=> $user, 'roles'=>$roles,'listRoleIds'=>$listRoleIds]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
      
        $user = $this->userModel->findOrFail($id);
        $image = $request->file('image');

        $dataUpdate = $request->all();

        $dataUpdate['image'] = $this->updateImage($image, $this->path, $user->image);

        $user->update($dataUpdate);
        $user->syncRoles($request->roles);

        return redirect()->route('users.index')->with('message', 'sửa user thanh cong'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->route('users.index')->with('message', 'Xóa user thành công!');
    }
}
