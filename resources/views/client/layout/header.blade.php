<header>
	<div class="top">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="top-menu">
						<ul>
							<li><a href="#">Login |</a></li><li><a href="#">Register</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="main-header">
		<div class="container">
			<div class="row">
				<div class="col-6 col-xs-6 col-sm-6 col-md-3 col-lg-3 order-md-0 order-0">
					<div class="logo">
						<a href="#"><img src="{{ asset('client/images/anhshop.png')}}" alt=""></a>
						<h1>Website bán hàng</h1>
					</div>
				</div>
				<div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 order-md-1 order-2">
					<div class="form-seach-product">
						<form action="/" method="GET" role="form">
							<select name="" id="input" class="form-control" required="required">
								<option value="">Chọn danh mục</option>
								<option value=""></option>
								</select>
							<div class="input-seach">
								<input type="text" name="s" id="" class="form-control">
								<button type="submit" class="btn-search-pro"><i class="fa fa-search"></i></button>
							</div>
							<div class="clear"></div>
						</form>
					</div>
				</div>
				<div class="col-6 col-xs-6 col-sm-6 col-md-3 col-lg-3 order-md-2 order-1" style="text-align: right">
					<a href="#" class="icon-cart">
						<div class="icon">
							<i class="fa fa-shopping-cart" aria-hidden="true"></i>
							<span>0</span>
						</div>
						<div class="info-cart">
							<p>Giỏ hàng</p>
							<span>0đ</span>
						</div>
						<span class="clear"></span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="main-menu-header">
		<div class="container">
			<div id="nav-menu">
				<ul>
					<li class="current-menu-item"><a href="#">Trang chủ</a></li>
					<li><a href="#">Giới thiệu</a></li>
					<li>
						<a href="#">Sản phẩm</a>
						<ul>
							<li><a href="#">Điện thoại</a></li>
							<li><a href="#">Laptop</a></li>
							<li><a href="#">Sản phẩm khác</a></li>
						</ul>
					</li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</header>