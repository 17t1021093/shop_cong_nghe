
<!DOCTYPE html>
<html lang="vi">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Website bán hàng đơn giản</title>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i">
		@include('client.layout.style')
	</head>
	<body>
		<div id="wallpaper">
		@include('client.layout.header')
			<div id="content">
				<div class="container">
					<div class="slider">
						<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
						  <div class="carousel-inner">
						    <div class="carousel-item active">
						      <img class="d-block w-100" src="{{ asset('client/images/banner-01.png')}}" alt="First slide">
						    </div>
						    <div class="carousel-item">
						      <img class="d-block w-100" src="{{ asset('client/images/baner-02.png')}}" alt="Second slide">
						    </div>
						  </div>
						  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					</div>
				</div>
				<div class="product-box">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 order-lg-0 order-1">
								<div class="sidebar">
									<div class="category-box">
										<h3>Danh mục sản phẩm</h3>
										@foreach($category as $value)
										<div class="content-cat">
											<ul>
												<li><i class="fa fa-angle-right"></i> <a href="#">{{$value->name}}</a></li>
											</ul>
										</div>
										@endforeach
									</div>
									<div class="widget">
										<h3>
											<i class="fa fa-bars"></i>
											Tin tức
										</h3>
										<div class="content-w">
											<ul>
												<li>
													<a href="#"><img src="images/news.jpg" alt=""></a>
													<h4><a href="#">Thương hiệu đồng hồ thông minh Sinophy của nước nào?</a></h4>
													<div class="clear"></div>
												</li>
												<li>
													<a href="#"><img src="images/news.jpg" alt=""></a>
													<h4><a href="#">Thương hiệu đồng hồ thông minh Sinophy của nước nào?</a></h4>
													<div class="clear"></div>
												</li>
												<li>
													<a href="#"><img src="images/news.jpg" alt=""></a>
													<h4><a href="#">Thương hiệu đồng hồ thông minh Sinophy của nước nào?</a></h4>
													<div class="clear"></div>
												</li>
												<li>
													<a href="#"><img src="images/news.jpg" alt=""></a>
													<h4><a href="#">Thương hiệu đồng hồ thông minh Sinophy của nước nào?</a></h4>
													<div class="clear"></div>
												</li>
												<li>
													<a href="#"><img src="images/news.jpg" alt=""></a>
													<h4><a href="#">Thương hiệu đồng hồ thông minh Sinophy của nước nào?</a></h4>
													<div class="clear"></div>
												</li>
											</ul>
										</div>
									</div>
									<div class="widget">
										<h3>
											<i class="fa fa-bars"></i>
											Quảng cáo
										</h3>
										<div class="content-banner">
											<a href="#">
												<img src="images/banner.png" alt="">
											</a>
										</div>
									</div>
									<div class="widget">
										<h3>
											<i class="fa fa-facebook"></i>
											Facebook
										</h3>
										<div class="content-fb">
											<div class="fb-page" data-href="https://www.facebook.com/huykiradotnet/" data-tabs="timeline" data-width="" data-height="200" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 order-lg-1 order-0">
								<div class="product-section">
									<h2 class="title-product">
										<a href="#" class="title">Sản phẩm nổi bật</a>
										<div class="bar-menu"><i class="fa fa-bars"></i></div>
										<div class="list-child">
											<ul>
												<li><a href="#">Điện thoại</a></li>
												<li><a href="#">Máy tính bảng</a></li>
												<li><a href="#">Laptop</a></li>
												<li><a href="#">Phụ kiện</a></li>
											</ul>
										</div>
										<div class="clear"></div>
									</h2>
									<div class="content-product-box">
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
												<div class="item-product">
													<div class="thumb">
														<a href="#"><img src="{{ asset('client/images/dth1.jpg')}}" alt=""></a>
														<span class="sale">Giảm <br>15%</span>
														<div class="action">
															<a href="#" class="buy"><i class="fa fa-cart-plus"></i> Mua ngay</a>
															<a href="#" class="like"><i class="fa fa-heart"></i> Yêu thích</a>
															<div class="clear"></div>
														</div>
													</div>
													<div class="info-product">
														<h4><a href="#">Điện thoại iPhone Xs Max 256GB</a></h4>
														<div class="price">
															<span class="price-current">28.990.000₫</span>
															<span class="price-old">33.990.000₫</span>
														</div>
														<a href="#" class="view-more">Xem chi tiết</a>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
												<div class="item-product">
													<div class="thumb">
														<a href="#"><img src="{{ asset('client/images/dth2.jpg')}}" alt=""></a>
														<span class="sale">Giảm <br>10%</span>
														<div class="action">
															<a href="#" class="buy"><i class="fa fa-cart-plus"></i> Mua ngay</a>
															<a href="#" class="like"><i class="fa fa-heart"></i> Yêu thích</a>
															<div class="clear"></div>
														</div>
													</div>
													<div class="info-product">
														<h4><a href="#">Điện thoại OPPO A5 (2020) 128GB</a></h4>
														<div class="price">
															<span class="price-current">4.790.000₫</span>
															<span class="price-old">4.790.000₫</span>
														</div>
														<a href="#" class="view-more">Xem chi tiết</a>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
												<div class="item-product">
													<div class="thumb">
														<a href="#"><img src="{{ asset('client/images/dth3.jpg')}}" alt=""></a>
														<span class="sale">Giảm <br>15%</span>
														<div class="action">
															<a href="#" class="buy"><i class="fa fa-cart-plus"></i> Mua ngay</a>
															<a href="#" class="like"><i class="fa fa-heart"></i> Yêu thích</a>
															<div class="clear"></div>
														</div>
													</div>
													<div class="info-product">
														<h4><a href="#">Điện thoại iPhone Xs Max 256GB</a></h4>
														<div class="price">
															<span class="price-current">28.990.000₫</span>
															<span class="price-old">33.990.000₫</span>
														</div>
														<a href="#" class="view-more">Xem chi tiết</a>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
												<div class="item-product">
													<div class="thumb">
														<a href="#"><img src="{{ asset('client/images/dth4.jpg')}}" alt=""></a>
														<span class="sale">Giảm <br>10%</span>
														<div class="action">
															<a href="#" class="buy"><i class="fa fa-cart-plus"></i> Mua ngay</a>
															<a href="#" class="like"><i class="fa fa-heart"></i> Yêu thích</a>
															<div class="clear"></div>
														</div>
													</div>
													<div class="info-product">
														<h4><a href="#">Điện thoại OPPO A5 (2020) 128GB</a></h4>
														<div class="price">
															<span class="price-current">4.790.000₫</span>
															<span class="price-old">4.790.000₫</span>
														</div>
														<a href="#" class="view-more">Xem chi tiết</a>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
												<div class="item-product">
													<div class="thumb">
														<a href="#"><img src="images/sp2.png" alt=""></a>
														<span class="sale">Giảm <br>10%</span>
														<div class="action">
															<a href="#" class="buy"><i class="fa fa-cart-plus"></i> Mua ngay</a>
															<a href="#" class="like"><i class="fa fa-heart"></i> Yêu thích</a>
															<div class="clear"></div>
														</div>
													</div>
													<div class="info-product">
														<h4><a href="#">Điện thoại OPPO A5 (2020) 128GB</a></h4>
														<div class="price">
															<span class="price-current">4.790.000₫</span>
															<span class="price-old">4.790.000₫</span>
														</div>
														<a href="#" class="view-more">Xem chi tiết</a>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
												<div class="item-product">
													<div class="thumb">
														<a href="#"><img src="images/sp.png" alt=""></a>
														<span class="sale">Giảm <br>15%</span>
														<div class="action">
															<a href="#" class="buy"><i class="fa fa-cart-plus"></i> Mua ngay</a>
															<a href="#" class="like"><i class="fa fa-heart"></i> Yêu thích</a>
															<div class="clear"></div>
														</div>
													</div>
													<div class="info-product">
														<h4><a href="#">Điện thoại iPhone Xs Max 256GB</a></h4>
														<div class="price">
															<span class="price-current">28.990.000₫</span>
															<span class="price-old">33.990.000₫</span>
														</div>
														<a href="#" class="view-more">Xem chi tiết</a>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
												<div class="item-product">
													<div class="thumb">
														<a href="#"><img src="images/sp2.png" alt=""></a>
														<span class="sale">Giảm <br>10%</span>
														<div class="action">
															<a href="#" class="buy"><i class="fa fa-cart-plus"></i> Mua ngay</a>
															<a href="#" class="like"><i class="fa fa-heart"></i> Yêu thích</a>
															<div class="clear"></div>
														</div>
													</div>
													<div class="info-product">
														<h4><a href="#">Điện thoại OPPO A5 (2020) 128GB</a></h4>
														<div class="price">
															<span class="price-current">4.790.000₫</span>
															<span class="price-old">4.790.000₫</span>
														</div>
														<a href="#" class="view-more">Xem chi tiết</a>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
												<div class="item-product">
													<div class="thumb">
														<a href="#"><img src="images/sp.png" alt=""></a>
														<span class="sale">Giảm <br>15%</span>
														<div class="action">
															<a href="#" class="buy"><i class="fa fa-cart-plus"></i> Mua ngay</a>
															<a href="#" class="like"><i class="fa fa-heart"></i> Yêu thích</a>
															<div class="clear"></div>
														</div>
													</div>
													<div class="info-product">
														<h4><a href="#">Điện thoại iPhone Xs Max 256GB</a></h4>
														<div class="price">
															<span class="price-current">28.990.000₫</span>
															<span class="price-old">33.990.000₫</span>
														</div>
														<a href="#" class="view-more">Xem chi tiết</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<a href="#"><img src="https://phongtrodn.com/wp-content/uploads/2020/02/huykira.png" alt=""></a>
								<br>
								<br>
								<div class="product-section">
									<h2 class="title-product">
										<a href="#" class="title">Điện thoại</a>
										<div class="bar-menu"><i class="fa fa-bars"></i></div>
										<div class="list-child">
											<ul>
												<li><a href="#">Iphone</a></li>
												<li><a href="#">Samsung</a></li>
												<li><a href="#">Sony</a></li>
												<li><a href="#">HTC</a></li>
											</ul>
										</div>
										<div class="clear"></div>
									</h2>
									<div class="content-product-box">
										<div class="row">
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
												<div class="item-product">
													<div class="thumb">
														<a href="#"><img src="images/sp.png" alt=""></a>
														<span class="sale">Giảm <br>15%</span>
														<div class="action">
															<a href="#" class="buy"><i class="fa fa-cart-plus"></i> Mua ngay</a>
															<a href="#" class="like"><i class="fa fa-heart"></i> Yêu thích</a>
															<div class="clear"></div>
														</div>
													</div>
													<div class="info-product">
														<h4><a href="#">Điện thoại iPhone Xs Max 256GB</a></h4>
														<div class="price">
															<span class="price-current">28.990.000₫</span>
															<span class="price-old">33.990.000₫</span>
														</div>
														<a href="#" class="view-more">Xem chi tiết</a>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
												<div class="item-product">
													<div class="thumb">
														<a href="#"><img src="images/sp2.png" alt=""></a>
														<span class="sale">Giảm <br>10%</span>
														<div class="action">
															<a href="#" class="buy"><i class="fa fa-cart-plus"></i> Mua ngay</a>
															<a href="#" class="like"><i class="fa fa-heart"></i> Yêu thích</a>
															<div class="clear"></div>
														</div>
													</div>
													<div class="info-product">
														<h4><a href="#">Điện thoại OPPO A5 (2020) 128GB</a></h4>
														<div class="price">
															<span class="price-current">4.790.000₫</span>
															<span class="price-old">4.790.000₫</span>
														</div>
														<a href="#" class="view-more">Xem chi tiết</a>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
												<div class="item-product">
													<div class="thumb">
														<a href="#"><img src="images/sp.png" alt=""></a>
														<span class="sale">Giảm <br>15%</span>
														<div class="action">
															<a href="#" class="buy"><i class="fa fa-cart-plus"></i> Mua ngay</a>
															<a href="#" class="like"><i class="fa fa-heart"></i> Yêu thích</a>
															<div class="clear"></div>
														</div>
													</div>
													<div class="info-product">
														<h4><a href="#">Điện thoại iPhone Xs Max 256GB</a></h4>
														<div class="price">
															<span class="price-current">28.990.000₫</span>
															<span class="price-old">33.990.000₫</span>
														</div>
														<a href="#" class="view-more">Xem chi tiết</a>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
												<div class="item-product">
													<div class="thumb">
														<a href="#"><img src="images/sp2.png" alt=""></a>
														<span class="sale">Giảm <br>10%</span>
														<div class="action">
															<a href="#" class="buy"><i class="fa fa-cart-plus"></i> Mua ngay</a>
															<a href="#" class="like"><i class="fa fa-heart"></i> Yêu thích</a>
															<div class="clear"></div>
														</div>
													</div>
													<div class="info-product">
														<h4><a href="#">Điện thoại OPPO A5 (2020) 128GB</a></h4>
														<div class="price">
															<span class="price-current">4.790.000₫</span>
															<span class="price-old">4.790.000₫</span>
														</div>
														<a href="#" class="view-more">Xem chi tiết</a>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
												<div class="item-product">
													<div class="thumb">
														<a href="#"><img src="images/sp2.png" alt=""></a>
														<span class="sale">Giảm <br>10%</span>
														<div class="action">
															<a href="#" class="buy"><i class="fa fa-cart-plus"></i> Mua ngay</a>
															<a href="#" class="like"><i class="fa fa-heart"></i> Yêu thích</a>
															<div class="clear"></div>
														</div>
													</div>
													<div class="info-product">
														<h4><a href="#">Điện thoại OPPO A5 (2020) 128GB</a></h4>
														<div class="price">
															<span class="price-current">4.790.000₫</span>
															<span class="price-old">4.790.000₫</span>
														</div>
														<a href="#" class="view-more">Xem chi tiết</a>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
												<div class="item-product">
													<div class="thumb">
														<a href="#"><img src="images/sp.png" alt=""></a>
														<span class="sale">Giảm <br>15%</span>
														<div class="action">
															<a href="#" class="buy"><i class="fa fa-cart-plus"></i> Mua ngay</a>
															<a href="#" class="like"><i class="fa fa-heart"></i> Yêu thích</a>
															<div class="clear"></div>
														</div>
													</div>
													<div class="info-product">
														<h4><a href="#">Điện thoại iPhone Xs Max 256GB</a></h4>
														<div class="price">
															<span class="price-current">28.990.000₫</span>
															<span class="price-old">33.990.000₫</span>
														</div>
														<a href="#" class="view-more">Xem chi tiết</a>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
												<div class="item-product">
													<div class="thumb">
														<a href="#"><img src="images/sp2.png" alt=""></a>
														<span class="sale">Giảm <br>10%</span>
														<div class="action">
															<a href="#" class="buy"><i class="fa fa-cart-plus"></i> Mua ngay</a>
															<a href="#" class="like"><i class="fa fa-heart"></i> Yêu thích</a>
															<div class="clear"></div>
														</div>
													</div>
													<div class="info-product">
														<h4><a href="#">Điện thoại OPPO A5 (2020) 128GB</a></h4>
														<div class="price">
															<span class="price-current">4.790.000₫</span>
															<span class="price-old">4.790.000₫</span>
														</div>
														<a href="#" class="view-more">Xem chi tiết</a>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
												<div class="item-product">
													<div class="thumb">
														<a href="#"><img src="images/sp.png" alt=""></a>
														<span class="sale">Giảm <br>15%</span>
														<div class="action">
															<a href="#" class="buy"><i class="fa fa-cart-plus"></i> Mua ngay</a>
															<a href="#" class="like"><i class="fa fa-heart"></i> Yêu thích</a>
															<div class="clear"></div>
														</div>
													</div>
													<div class="info-product">
														<h4><a href="#">Điện thoại iPhone Xs Max 256GB</a></h4>
														<div class="price">
															<span class="price-current">28.990.000₫</span>
															<span class="price-old">33.990.000₫</span>
														</div>
														<a href="#" class="view-more">Xem chi tiết</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer>
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<div class="box-footer info-contact">
								<h3>Thông tin liên hệ</h3>
								<div class="content-contact">
									<p>Website chuyên cung cấp thiết bị điện tử hàng đầu Việt Nam</p>
									<p>
										<strong>Địa chỉ:</strong> 457/44 Tôn Đức Thắng, Liên Chiểu, Đà Nẵng
									</p>
									<p>
										<strong>Email: </strong> thietkeweb43.com@gmail.com
									</p>
									<p>
										<strong>Điện thoại: </strong> 0358949xxx
									</p>
									<p>
										<strong>Website: </strong> https://huykira.net
									</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<div class="box-footer info-contact">
								<h3>Thông tin khác</h3>
								<div class="content-list">
									<ul>
										<li><a href="#"><i class="fa fa-angle-double-right"></i> Chính sách bảo mật</a></li>
										<li><a href="#"><i class="fa fa-angle-double-right"></i> Chính sách đổi trả</a></li>
										<li><a href="#"><i class="fa fa-angle-double-right"></i> Phí vẫn chuyển</a></li>
										<li><a href="#"><i class="fa fa-angle-double-right"></i> Hướng dẫn thanh toán</a></li>
										<li><a href="#"><i class="fa fa-angle-double-right"></i> Chương trình khuyến mãi</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<div class="box-footer info-contact">
								<h3>Form liên hệ</h3>
								<div class="content-contact">
									<form action="/" method="GET" role="form">
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<input type="text" name="" id="" class="form-control" placeholder="Họ và Tên">
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
												<input type="email" name="" id="" class="form-control" placeholder="Địa chỉ mail">
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
												<input type="text" name="" id="" class="form-control" placeholder="Số điện thoại">
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<input type="text" name="" id="" class="form-control" placeholder="Tiêu đề">
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
											</div>
										</div>
										<button type="submit" class="btn-contact">Liên hệ ngay</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</footer>
		</div>
		@include('client.layout.javascript')
		<div id="fb-root"></div>
		<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v6.0"></script>
	</body>
</html>