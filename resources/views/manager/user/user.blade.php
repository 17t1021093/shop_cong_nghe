@extends('manager.layout.app')

@section('title', 'Users')

@section('content')
<a class="btn btn-success" href="{{route('users.create')}}">Create New User</a>
    @if(session('message'))
        <h1>{{session('message')}} </h1>
    @endif
    

    <table  class="table table-striped mt-3">
        <thead class="thead-dark">
            <tr>
                <td scope="col">UserID</td>
                <td scope="col">Name</td>
                <td scope="col">Role</td>
                <td scope="col">Image</td>
                <td></td>
            </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td scope="row">{{$loop->iteration}}</td>
                <td>{{$user->name}}</td>
                <td>
                    @if($user->roles->count() >0)
                        @foreach($user->roles as $role)
                            {{$role->display_name .','}}
                        @endforeach
                    @endif
                </td>
                <td>
                    <img width="100px" height="100px" src="{{asset('upload/'.$user->image)}}" alt="">
                </td>
                <td class="table-buttons" style="text-align: right;">
                    <!-- <a class="btn btn-warning text-white" href="{{route('users.edit', $user->id)}}">Edit</a>
                    <button class="btn btn-danger">DELETE</button> -->
                    <a href="{{ route('users.edit', $user) }}" class="btn btn-primary">
                    <i class="fa fa-pencil"></i>
                    </a>
                    <form METHOD="post" ACTION="{{ route('users.destroy', $user) }}" style="display: contents;">
                    @csrf
                    @method('DELETE')
                        <button type="submit" class="btn btn-danger">
                        <i class="fa fa-trash"></i>
                        </button>
                    </form>

                </td>
            </tr>
        @endforeach
    </table>


    <div>
        {{ $users->links() }}
    </div>

@endsection
