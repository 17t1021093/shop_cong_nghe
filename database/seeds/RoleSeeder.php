<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleManagerUser = \App\Models\Role::updateOrCreate([
            'name'=>'manager-user-in-admin',
            'display_name'=>'Quản lý người dùng',
        ]);

        $roleManagerUser = \App\Models\Role::updateOrCreate([
            'name'=>'manager-category-in-admin',
            'display_name'=>'Quản lý category',
        ]);

        $roleManagerUser = \App\Models\Role::updateOrCreate([
            'name'=>'manager-product-in-admin',
            'display_name'=>'Quản lý product',
        ]);

        $roleManagerUser = \App\Models\Role::updateOrCreate([
            'name'=>'manager-order-in-admin',
            'display_name'=>'Quản lý order',
        ]);

        $roleManagerUser = \App\Models\Role::updateOrCreate([
            'name'=>'super-admin',
            'display_name'=>'admin',
        ]);

        $curdUserPermission = \App\Models\Permission::updateOrCreate([
            'name'=>'curd-user-in-admin',
            'display_name'=>'Quan ly nguoi dung',
        ]);

        \App\Models\Permission::updateOrCreate([
            'name'=>'can-update-status-order-in-admin',
            'display_name'=>'thay doi trang thai don hang',
        ]);

        $roleManagerUser->givePermissionTo($curdUserPermission->name);

       $userAdmin = \App\User::where('email','cd12@hue')->first();

       if (!$userAdmin) {
          $userAdmin = \App\User::create([
            'name' => 'shop deha',
            'email' => 'admin12@gmail.com',
            'password' => \Illuminate\support\facades\Hash::make('123123123'),
            'phone' => '342535534',
            'address' => 'dwqdwqdwqd',
            'gender' => '',
            'image' => '',

          ]);
       }

       if (!$userAdmin->hasRole('super-admin')) {
           $userAdmin->assignRole('super-admin');
       }
    }
}
