@extends('manager.layout.app')

@section('title', 'Product')

@section('content')
<a class="btn btn-success" href="{{route('products.create')}}">Create New Product</a>
    @if(session('message'))
        <h1>{{session('message')}} </h1>
    @endif

    <table  class="table table-striped mt-3">
    <thead class="thead-dark">
        <tr>
            <td scope="col">STT</td>
            <td scope="col">Name</td>
            <td scope="col">Price</td>
            <td scope="col">Image</td>
            <td scope="col">Category</td>
            <td></td>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $productIem)
            
            <tr>
                <td scope="row">{{$productIem->id}}</td>
                <td>{{$productIem->name}}</td>
                <td>{{$productIem->price}}</td>
                
                <td>
                    <img width="100px" height="100px" src="{{asset('upload/'.$productIem->image)}}" alt="">
                </td>
                <td>{{optional($productIem->category)->name}}</td>

                <td class="table-buttons">
                    <a class="btn btn-primary" href="{{ route('products.edit', $productIem) }}"><i class="fa fa-pencil"></i></a>
                    <form METHOD="post" ACTION="{{ route('products.destroy', $productIem) }}" style="display: contents;">
                    @csrf
                    @method('DELETE')
                        <button type="submit" class="btn btn-danger">
                        <i class="fa fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    <div>
       
    </div>

@endsection
