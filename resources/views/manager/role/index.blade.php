@extends('manager.layout.app')

@section('title', 'Role')

@section('content')
    <a class="btn btn-success" href="{{route('roles.create')}}">Create New Role</a>

    <table  class="table table-striped mt-3">
        <thead class="thead-dark">
        <tr>
            <td scope="col">STT</td>
            <td scope="col">name</td>
            <td scope="col">display name</td>
            <td></td>
        </tr>
        </thead>
        <tbody>
        @foreach($roles as $item)
            <tr>
                <td scope="row">{{$loop->iteration}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->display_name}}</td>

                <td class="table-buttons">
                    <a class="btn btn-primary" href="{{route('roles.edit', $item)}}"><i class="fa fa-pencil"></i></a>

                    <form METHOD="post" ACTION="{{ route('roles.destroy', $item) }}" style="display: contents;">
                    @csrf
                    @method('DELETE')
                        <button type="submit" class="btn btn-danger">
                        <i class="fa fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>


    <div>
        {{ $roles->links() }}
    </div>

@endsection
